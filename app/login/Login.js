import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    StatusBar,
    TextInput,
    Dimensions,
} from 'react-native';
import { Card, Button, Text, Icon } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient';

const DEVICE = Dimensions.get('window')

export default class Login extends React.Component {
    render() {
        const { navigation } = this.props
        return (
            <View style={styles.container}>
                <LinearGradient
                    colors={['#4c669f', '#3b5998', '#192f6a']}
                    style={styles.linearGradient}>

                    <StatusBar translucent={false} backgroundColor={"grey"} />
                    <View style={styles.content}>

                        <Icon
                            raised
                            reverse
                            name='shopping-basket'
                            type='font-awesome'
                            color='#da2'
                            size={40} />

                        <Text h3 style={styles.titleLogin}>Login</Text>
                        <View style={styles.textContent}>
                            <Text style={{color: 'white'}}>Username</Text>
                            <TextInput style={styles.textInput}></TextInput>
                        </View>
                        <View style={styles.textContent}>
                            <Text style={{color: 'white'}}>Password</Text>
                            <TextInput style={styles.textInput} secureTextEntry={true}></TextInput>
                        </View>
                        <TouchableOpacity style={styles.button}
                        onPress={() => navigation.push('Home')}>
                            <Text style={styles.textBtn}>Masuk</Text>
                        </TouchableOpacity>
                        <Text style={{ color: '#3EC6FF', marginVertical: 10 }}>Atau</Text>
                        <TouchableOpacity style={[styles.button, { backgroundColor: '#ee5577' }]}>
                            <Text style={styles.textBtn}>Daftar</Text>
                        </TouchableOpacity>

                        <View style={styles.footer}>
                            <Text style={{color: 'white', fontSize: 12}}>LapakGue Mobile Apps</Text>
                            <Text style={{color: 'white', fontSize: 12}}>Version 1.0</Text>
                        </View>
                    </View>

                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
        width: DEVICE.width
    },
    linearGradient: {
        flex: 1,
        padding: 15
    },
    img: {
        marginVertical: 10,
        resizeMode: "stretch"
    },
    content: {
        paddingTop: 20,
        paddingHorizontal: 20,
        flexDirection: 'column',
        alignItems: "center"
    },
    textContent: {
        marginBottom: 20,
        width: '100%'
    },
    textInput: {
        marginTop: 5,
        paddingHorizontal: 10,
        borderWidth: 1,
        height: 40,
        borderColor: '#BCBCBC',
        backgroundColor: '#DDDDDD',
        borderRadius: 5
    },
    titleLogin: {
        height: 50,
        fontWeight: "bold",
        marginTop: 20,
        marginBottom: 15,
        color: 'white'
    },
    button: {
        width: '50%',
        paddingHorizontal: 10,
        backgroundColor: '#3EC6FF',
        alignItems: "center",
        justifyContent: "center",
        elevation: 4,
        borderRadius: 30,
        height: 35,
    },
    textBtn: {
        color: 'white'
    },
    footer: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: '17%'
    }
})
