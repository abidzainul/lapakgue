import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

import Login from '../login/Login'
import Account from '../fitur/account/Account'
import Dashboard from '../fitur/dashboard/Dashboard'
import Inbox from '../fitur/inbox/Inbox'
import Cart from '../fitur/cart/Cart'
import About from '../fitur/account/About'

const TabsScreen = ({ navigation }) => (
    <Tab.Navigator
        initialRouteName="Dashboard"
        tabBarOptions={{
            activeTintColor: '#e91e63',
        }}>
        <Tab.Screen
            name="Dashboard"
            component={Dashboard}
            options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color, size }) => (
                    <Icon name="home" color={color} size={size} />
                ),
            }}
        />
        <Tab.Screen
            name="Cart"
            component={Cart}
            options={{
                tabBarLabel: 'Cart',
                tabBarIcon: ({ color, size }) => (
                    <Icon name="cart" color={color} size={size} />
                ),
            }}
        />
        <Tab.Screen
            name="Inbox"
            component={Inbox}
            options={{
                tabBarLabel: 'Inbox',
                tabBarIcon: ({ color, size }) => (
                    <Icon name="email" color={color} size={size} />
                ),
            }}
        />
        <Tab.Screen
            name="Account"
            component={Account}
            options={{
                tabBarLabel: 'Account',
                tabBarIcon: ({ color, size }) => (
                    <Icon name="account" color={color} size={size} />
                ),
            }}
        />
    </Tab.Navigator>
)


export default () => (
    <NavigationContainer>
        <Stack.Navigator initialRouteName='Login'>
            <Stack.Screen name="Home" component={TabsScreen} options={{ headerShown:false }}/>
            <Stack.Screen name="Login" component={Login} options={{ headerShown:false }} />
            <Stack.Screen name="About" component={About} />
        </Stack.Navigator>
    </NavigationContainer>
)