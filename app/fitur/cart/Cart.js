import React, { Component } from 'react';
import { StyleSheet, View, FlatList, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Header } from 'react-native-elements';

export default class Cart extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    statusBarProps={{ barStyle: 'light-content' }}
                    barStyle="light-content"
                    leftComponent={{ text: 'Cart', style: { color: '#fff', fontSize: 18 } }}
                />
                <View style={styles.content}>
                    <Icon name="cart" size={100} color="#dedede" />
                    <Text style={{ color: '#656565' }}>Item tidak ditemukan</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
})