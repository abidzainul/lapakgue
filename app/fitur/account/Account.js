import React from 'react';
import { Card } from 'react-native-elements';
import {
    View,
    Dimensions,
    FlatList,
    StyleSheet,
    StatusBar,
    Text,
    ScrollView,
    TouchableOpacity
} from 'react-native';

const DEVICE = Dimensions.get('window')

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Account extends React.Component {
    render() {
        const { navigation } = this.props
        return (
            <ScrollView  style={styles.container}>
                <StatusBar translucent={false} backgroundColor={"grey"} />

                <View style={styles.bgHeader}></View>

                <View style={styles.profile}>
                    <Icon name="account-circle" size={70} color="#dedede" />
                    <View style={styles.textProfile}>
                        <Text style={{ fontSize: 18, color: 'white', fontWeight: 'bold' }}>Abid Zainul</Text>
                        <Text style={{ fontSize: 14, color: 'white' }}>@abidzainul</Text>
                    </View>
                    <View style={{padding: 10}}>
                        <Icon name="pencil" size={25} color="#dedede" />
                    </View>
                </View>

                <Card containerStyle={styles.card}>
                    <View style={styles.saldo}>
                        <View style={styles.itemSaldo}>
                            <Text style={{ color: '#656565' }}>Saldo</Text>
                            <Text style={styles.textSaldo}>Rp. 110,000</Text>
                        </View>
                        <View style={styles.itemSaldo}>
                            <Text style={{ color: '#656565' }}>Voucher</Text>
                            <Text style={styles.textSaldo}>3</Text>
                        </View>
                        <View style={styles.itemSaldo}>
                            <Text style={{ color: '#656565' }}>Point</Text>
                            <Text style={styles.textSaldo}>1,700</Text>
                        </View>
                    </View>
                </Card>

                <View style={styles.order}>
                    <Text style={styles.labelOrder}>Pesanan Saya</Text>
                    <View style={styles.menuOrder}>
                        <TouchableOpacity style={styles.itemSaldo}>
                            <Icon name="wallet-outline" size={40} color="#66d9ff" />
                            <Text style={{ fontSize: 12 }}>Belum</Text>
                            <Text style={{ fontSize: 12 }}>Dibayar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemSaldo}>
                            <Icon name="truck-outline" size={40} color="#66d9ff" />
                            <Text style={{ fontSize: 12 }}>Menunggu</Text>
                            <Text style={{ fontSize: 12 }}>Diterima</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemSaldo}>
                            <Icon name="tooltip-edit-outline" size={40} color="#66d9ff" />
                            <Text style={{ fontSize: 12 }}>Menunggu</Text>
                            <Text style={{ fontSize: 12 }}>Ulasan</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemSaldo}>
                            <Icon name="package-variant-closed" size={40} color="#66d9ff" />
                            <Text style={{ fontSize: 12 }}>Retur</Text>
                            <Text style={{ fontSize: 12 }}>Produk</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.menuLain}>
                    <Text style={styles.labelMenuLain}>Menu Lainnya</Text>
                    <View style={styles.listMenuLain}>
                        <TouchableOpacity 
                        style={styles.itemMenuLain}
                        onPress={() => this.props.navigation.navigate('About')} >
                            <Icon name="information-outline" size={25} color="#4B0082" />
                            <Text style={{ flex: 1, marginStart: 10 }}>About</Text>
                            <Icon name="chevron-right" size={25} color="grey" />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemMenuLain}>
                            <Icon name="head-question" size={25} color="#4B0082" />
                            <Text style={{ flex: 1, marginStart: 10 }}>Help</Text>
                            <Icon name="chevron-right" size={25} color="grey" />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemMenuLain}>
                            <Icon name="bell-circle-outline" size={25} color="#4B0082" />
                            <Text style={{ flex: 1, marginStart: 10 }}>Notifikasi</Text>
                            <Icon name="chevron-right" size={25} color="grey" />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemMenuLain}>
                            <Icon name="cog-outline" size={25} color="#4B0082" />
                            <Text style={{ flex: 1, marginStart: 10 }}>Setting</Text>
                            <Icon name="chevron-right" size={25} color="grey" />
                        </TouchableOpacity>
                        <TouchableOpacity 
                        style={styles.itemMenuLain}
                        onPress={() => navigation.navigate('Login')}>
                            <Icon name="logout" size={25} color="#FF0082" />
                            <Text style={{ flex: 1, marginStart: 10 }}>Logout</Text>
                            <Icon name="chevron-right" size={25} color="grey" />
                        </TouchableOpacity>
                    </View>
                </View>

            </ScrollView >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    profile: {
        flexDirection: 'row',
        paddingTop: 20,
        paddingBottom: 10,
        paddingHorizontal: 10,
        justifyContent: 'space-between',
    },
    textProfile: {
        justifyContent: 'center', 
        marginStart: 10,
        flex: 1
    },
    card: {
        elevation: 2,
        borderRadius: 10,
    },
    saldo: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemSaldo: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    textSaldo: {
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 5
    },
    menu: {
        flexDirection: 'column',
        marginTop: 20,
        marginBottom: 10,
        paddingHorizontal: 10
    },
    bgHeader: {
        position: 'absolute',
        backgroundColor: '#1966ff',
        height: 150,
        width: DEVICE.width
    },
    order: {
        backgroundColor: 'white',
        paddingBottom: 10, 
        marginBottom: 10,
        marginTop: 15
    },
    labelOrder: {
        fontSize: 18, 
        fontWeight: 'bold', 
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    menuOrder: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        paddingHorizontal: 15
    },
    labelMenuLain: {
        fontSize: 18, 
        fontWeight: 'bold', 
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    menuLain: {
        backgroundColor: 'white',
        marginBottom: 10,
        marginTop: 10
    },
    listMenuLain: {
        flexDirection: 'column',
        backgroundColor: 'white',
        paddingHorizontal: 15, 
        marginBottom: 10,
    },
    itemMenuLain: {
        flexDirection: 'row',
        backgroundColor: 'white',
        paddingVertical: 15,
        borderBottomWidth: 0.5,
        borderColor: '#dcdcdc',
        alignItems: 'center'
    },
});