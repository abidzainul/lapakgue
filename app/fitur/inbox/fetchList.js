import axios from 'axios';
import {
    fetchListBegin,
    fetchListSuccess,
    fetchListFailure
} from "./actions";

export function fetchList() {
    return async dispatch => {
        dispatch(fetchListBegin());
        try {
            const json = await axios.get('http://demo5479694.mockable.io/inbox')
                .then(res => {
                    if (res.status != 200) {
                        throw (res.error);
                    } else {
                        return res.data
                    }
                })
            console.log(json)
            dispatch(fetchListSuccess(json.data));
            return json.data;
        }
        catch (error) {
            return dispatch(fetchListFailure(error));
        }
    };
}
