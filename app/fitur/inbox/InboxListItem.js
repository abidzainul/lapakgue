import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class InboxListItem extends React.Component {
    render() {
        let data = this.props.data;
        console.log('data: ' + data)
        return (
            <View style={styles.container}>
                <Icon name="bell" size={35} color="#22a" />
                <View style={styles.content} >

                    <Text style={styles.textTitle}>{data.title}</Text>
                    <Text style={styles.textDesc}>{data.message}</Text>
                    <Text style={styles.textDate}>{data.date}</Text>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        padding: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: '#dedede'
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        marginStart: 10
    },
    textTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black'
    },
    textDesc: {
        fontSize: 12,
        color: 'grey'
    },
    textDate: {
        fontSize: 11,
        color: '#a9a9a9',
        marginTop: 8
    }
});