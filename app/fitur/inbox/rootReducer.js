import { combineReducers } from "redux";
import inbox from "./reducer";

export default combineReducers({
    inbox
});
