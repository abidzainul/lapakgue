export const FETCH_LIST_BEGIN = "FETCH_LIST_BEGIN";
export const FETCH_LIST_SUCCESS ="FETCH_LIST_SUCCESS";
export const FETCH_LIST_FAILURE ="FETCH_LIST_FAILURE";

export const fetchListBegin = () => ({
    type: FETCH_LIST_BEGIN
});

export const fetchListSuccess = data => ({
    type: FETCH_LIST_SUCCESS,
    payload: { data }
});

export const fetchListFailure = error => ({
    type: FETCH_LIST_FAILURE,
    payload: { error }
});