import React from "react";
import { connect } from "react-redux";
import { fetchList } from "./fetchList";
import { View, Text, FlatList } from "react-native";
import InboxListItem from "./InboxListItem";
import { Header } from 'react-native-elements';

import LoadingDialog from '../../component/loader'

class InboxView extends React.Component {
    componentDidMount() {
        this.props.dispatch(fetchList());
    }

    render() {
        const { error, loading, inbox } = this.props;

        if (error) {
            return <Text>Error! {error.message}</Text>
        }

        if (loading) {
            return <LoadingDialog />
        }

        return (
            <View style={{ flex: 1 }}>
                <Header
                    statusBarProps={{ barStyle: 'light-content' }}
                    barStyle="light-content"
                    leftComponent={{ text: 'Inbox', style: { color: '#fff', fontSize: 18 } }}
                />
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={inbox}
                        renderItem={(inbox) => <InboxListItem data={inbox.item} />}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    inbox: state.inbox.items,
    loading: state.inbox.loading,
    error: state.inbox.error
});

export default connect(mapStateToProps)(InboxView);
