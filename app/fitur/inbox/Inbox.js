import React from "react";
import { Provider } from "react-redux";

import InboxView from "./InboxView";

import store from './store'

export default class Inbox extends React.Component {
  render(){
    return (
        <Provider store={store}>
          <InboxView />
        </Provider>
      );
  }
}

