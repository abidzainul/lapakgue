import React from "react";
import { Provider } from "react-redux";

import DashboardView from "./DashboardView";

import store from './products/store'

export default class Dashboard extends React.Component {
  render(){
    return (
        <Provider store={store}>
          <DashboardView />
        </Provider>
      );
  }
}

