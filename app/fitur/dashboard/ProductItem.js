import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Card, Image, Icon } from 'react-native-elements';

export default class ProductItem extends React.Component {
    currencyFormat(num) {
      return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    render() {
        let data = this.props.data;
        return (
            <Card containerStyle={styles.card}
                image={{ uri: data.img }}
                imageStyle={{borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                <Text style={{ marginBottom: 5 }}>
                    {data.name}
                </Text>
                <Text style={{ fontSize: 12, color: '#e67', fontWeight: 'bold' }}>
                    {this.currencyFormat(parseInt(data.price))}
                </Text>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        width: 150,
        height: 220,
        elevation: 4,
        borderRadius: 5,
        marginBottom: 10
    },
    content: {
        flexDirection: 'column'
    },
    textTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black'
    },
    textPrice: {
        fontSize: 12,
        color: 'grey'
    },
    textFinalPrice: {
        fontSize: 12,
        color: '#bcbcbc'
    }
});