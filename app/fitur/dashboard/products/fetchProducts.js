import axios from 'axios';
import {
    fetchProductsBegin,
    fetchProductsSuccess,
    fetchProductsFailure
} from "./actions";

export function fetchProducts() {
    return async dispatch => {
        dispatch(fetchProductsBegin());
        try {
            const json = await axios.get('http://demo5479694.mockable.io/products')
                .then(res => {
                    if (res.status != 200) {
                        throw (res.error);
                    } else {
                        return res.data
                    }
                })
            console.log(json)
            dispatch(fetchProductsSuccess(json.data));
            return json.data;
        }
        catch (error) {
            return dispatch(fetchProductsFailure(error));
        }
    };
}
