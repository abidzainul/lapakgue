import React, { Component } from 'react';
import { 
    StyleSheet, 
    View,  
    StatusBar, 
    FlatList, 
    TextInput, 
    Dimensions,
    ScrollView,
} from 'react-native';
import { Card, Image, Button, Icon, Text, ListItem } from 'react-native-elements';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { fetchProducts } from "./products/fetchProducts";
import { connect } from "react-redux";

import IconCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

import ProductItem from './ProductItem'

import LoadingDialog from '../../component/loader'

const DEVICE = Dimensions.get('window')

import menus from './menus.json';

class DashboardView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeIndex: 0,
            carouselItems: [
                {
                    img: "https://picjumbo.com/wp-content/uploads/D0101543-1080x720.jpg",
                },
                {
                    img: "https://picjumbo.com/wp-content/uploads/DSC08355-1080x720.jpg",
                },
                {
                    img: "https://picjumbo.com/wp-content/uploads/DSC04471-1080x720.jpg",
                },
                {
                    img: "https://picjumbo.com/wp-content/uploads/DSC06878-1080x720.jpg",
                },
                {
                    img: "https://picjumbo.com/wp-content/uploads/DSC06796-1080x720.jpg",
                },
            ]
        };
    }
    componentDidMount() {
        this.props.dispatch(fetchProducts());
    }

    _renderItem({ item, index }) {
        return (
            <View style={{
                backgroundColor: 'white',
                height: 250,
            }}>
                <Image source={{ uri: item.img }} style={styles.itemImage} resizeMode='cover' />
            </View>

        )
    }

    get pagination() {
        const { activeSlide, entries } = this.state;
        return (
            <Pagination
                dotsLength={5}
                activeDotIndex={0}
                containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    marginHorizontal: 8,
                    backgroundColor: 'rgba(255, 255, 255, 0.92)'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        );
    }

    render() {
        const { error, loading, products } = this.props;

        if (error) {
            return <Text>Error! {error.message}</Text>
        }

        if (loading) {
            return <LoadingDialog />
        }

        const renderItem = ({ item }) => (
            <View style={styles.menuItem}>
                <Icon
                    raised
                    reverse
                    name={item.icon}
                    type='font-awesome'
                    color={item.color} />
                <Text style={{ fontSize: 12 }}>{item.title}</Text>
            </View>
        );

        return (
            <ScrollView style={styles.container}>
                <StatusBar translucent={false} backgroundColor={"grey"} />

                <View style={styles.content}>
                    <View >
                        <View style={{ flexDirection: 'column', justifyContent: 'center', }}>
                            <Carousel
                                layout={"default"}
                                ref={ref => this.carousel = ref}
                                data={this.state.carouselItems}
                                backgroundColor='grey'
                                sliderWidth={500}
                                itemWidth={500}
                                useScrollView={true}
                                renderItem={this._renderItem} />
                            <View style={{ position: 'absolute', }}>
                                {/* { this.pagination } */}
                            </View>
                        </View>
                        <Card containerStyle={[styles.card, { position: 'absolute' }]}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <TextInput
                                    placeholder='Search'
                                />
                                <IconCommunity name="magnify" size={25} color="#bcbcbc" />
                            </View>
                        </Card>
                    </View>

                    <View style={styles.menu}>
                        <FlatList
                            data={menus.menus}
                            renderItem={renderItem}
                            keyExtractor={item => item.id.toString()}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>

                    <View style={styles.popular}>
                        <ListItem
                        leftIcon={<Icon name='star' color='#fa4'></Icon>}
                        title='Popular'
                        rightIcon={<Button 
                            type='outline' 
                            buttonStyle={styles.btnMore} 
                            titleStyle={{fontSize: 10, color: '#d35'}}
                            title='More'/>}
                        titleStyle={styles.titleText}></ListItem>
                    <FlatList
                        data={products}
                        renderItem={(product) => <ProductItem data={product.item} />}
                        keyExtractor={item => item.id.toString()}
                        horizontal={true}
                    />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = state => ({
    products: state.products.items,
    loading: state.products.loading,
    error: state.products.error
});

export default connect(mapStateToProps)(DashboardView);

const styles = StyleSheet.create({
    container: {
    },
    content: {
        paddingBottom: 20,
        flexDirection: 'column'
    },
    itemImage: {
        height: 250,
    },
    card: {
        elevation: 2,
        borderRadius: 5,
        paddingVertical: 0,
        width: DEVICE.width - 30,
    },
    menu: {
        paddingVertical: 10,
        paddingStart: 15,
        backgroundColor: 'white'
    },
    menuItem: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5
    },
    titleText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    popular: {
        backgroundColor: 'white',
        marginVertical: 10,
        paddingTop: 10,
        paddingBottom: 20
    },
    btnMore: {
        borderRadius: 15,
        paddingVertical: 1,
        borderColor: '#d35',
        paddingHorizontal: 10,
        backgroundColor: '#fed'
    }
})